// $('.dropdown').hover(function() {
// 	$(this).find('.dropdown-menu').stop(true, true).delay(200).slideDown(200);
// }, function() {
// 	$(this).find('.dropdown-menu').stop(true, true).delay(200).slideUp(200);
// });
// enable to use nav on click

$(document).ready(function() {
    // $('#datatable thead tr').clone(true).appendTo( '#datatable thead' );
    // $('#datatable thead tr:eq(1) th').each( function (i) {
    //     var title = $(this).text();
    //     $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

    //     $( 'input', this ).on( 'keyup change', function () {
    //         if (table.column(i).search() !== this.value) {
    //             table
    //                 .column(i)
    //                 .search( this.value )
    //                 .draw();
    //         }
    //     } );
    // } );

       var hCols = [3, 4];
    // DataTable initialisation
    var table= $('#datatable').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        "stripeClasses": [],
        // "dom": "<'row'<'col-2'l><'col-5'p<br/>i><'col-3'B><'col-2'p<br/>i>>",
        "dom": "<'row greybg'<'col-4 align-self-center'l><'col-4 nobg'><'col-4 text-right nobg'B>>" + "<'row'<'col-12'tr>>" + "<'row'<'col-6'i><'col-6'p>>",
        "paging": true,
        "autoWidth": true,
        responsive: {
            details: {
                renderer: function ( api, rowIdx, columns ) {
                    var data = $.map( columns, function ( col, i ) {
                        return col.hidden ?
                            '<div class="col-4"><div class="d-flex justify-content-start"><label class="labeltitle">'+col.title+':'+'</label><div class="extrainfo">'+col.data+'</div></div></div>':
                            // '<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
                            //     '<td>'+col.title+':'+'</td> '+
                            //     '<td>'+col.data+'</td>'+
                            // '</tr>' :
                            '';
                    } ).join('');
     
                    return data ?
                        $('<div class="row"/>').append( data ) :
                        false;
                }
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   3
        } ],
        order: [ 1, 'asc' ],
        "buttons": [{
            extend: 'colvis',
            // collectionLayout: 'two-column',
            text: function() {
                var totCols = $('#datatable tr th').length;
                var hiddenCols = hCols.length;
                var shownCols = totCols - hiddenCols;
                return '<img src="src/img/visibility.svg">';
            },
            prefixButtons: [{
                extend: 'colvisGroup',
                text: 'Show all',
                show: ':hidden'
            }]
        }, {
            extend: 'collection',
            text: '<img src="src/img/saveas.svg">',
            buttons: [{
                    text: 'Excel',
                    extend: 'excelHtml5',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }
                }, {
                    text: 'CSV',
                    extend: 'csvHtml5',
                    fieldSeparator: ';',
                    exportOptions: {
                        columns: ':visible'
                    }
                }, {
                    text: 'PDF Portrait',
                    extend: 'pdfHtml5',
                    message: '',
                    exportOptions: {
                        columns: ':visible'
                    }
                }, {
                    text: 'PDF Landscape',
                    extend: 'pdfHtml5',
                    message: '',
                    orientation: 'landscape',
                    exportOptions: {
                        columns: ':visible'
                    },
                    
                }]
            }]
        ,oLanguage: {
            oPaginate: {
                sPrevious: '<span class="pagination-next">|<</span>',
                sNext: '<span class="pagination-previous">>|</span>'
            }
        },
        "initComplete": function(settings, json) {
            // Adjust hidden columns counter text in button -->
            $('#datatable').on('column-visibility.dt', function(e, settings, column, state) {
                var visCols = $('#datatable thead tr:first th').length;
                //Below: The minus 2 because of the 2 extra buttons Show all and Restore
                var tblCols = $('.dt-button-collection li[aria-controls=datatable] a').length - 2;
                $('.buttons-colvis[aria-controls=datatable] span').html('Columns (' + visCols + ' of ' + tblCols + ')');
                e.stopPropagation();
            });
        }
    });

    // to show row details
    $("#datatable tbody tr td .row-details").on("click", function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(formatRowDetails(row.data())).show();
            tr.addClass('shown');
        }
    });
     /* Formatting function for row details - modify as you need */
     function formatRowDetails(data) {
        // 'data' is the original data object for the row
        var defaultName = data.name ? data.name : "test info"; //set this to data.name if object exists
        var defaultInfo = data.info ? data.info : "This is a info";
        return '<div class="row">' +
            '<div class="col-4">' +
                '<div class="d-flex justify-content-start">' +
                    '<label class="labeltitle">Net Paid Amount</label>' +
                    '<div class="extrainfo">3 days ago</div>' +
                '</div>' +
            '</div>' + 
            '<div class="col-4">' +
                '<div class="d-flex justify-content-start">' +
                    '<label class="labeltitle">Payment Info</label>' +
                    '<div class="extrainfo">Nepal Investment Bank Limited</div>' +
                '</div>' +
            '</div>' + 
            '<div class="col-4">' +
                '<div class="d-flex justify-content-start">' +
                    '<label class="labeltitle">Status</label>' +
                    '<div class="extrainfo">Verified</div>' +
                '</div>' +
            '</div>' +
            '<div class="col-4">' +
                '<div class="d-flex justify-content-start">' +
                    '<label class="labeltitle">Payment Info</label>' +
                    '<div class="extrainfo">Nepal Investment Bank Limited</div>' +
                '</div>' +
            '</div>' +
            '<div class="col-4">' +
                '<div class="d-flex justify-content-start">' +
                    '<label class="labeltitle">Status</label>' +
                    '<div class="extrainfo">Verified</div>' +
                '</div>' +
            '</div>' +
            '<div class="col-4">' +
                '<div class="d-flex justify-content-start">' +
                    '<label class="labeltitle">Net Paid Amount</label>' +
                    '<div class="extrainfo">72700.00</div>' +
                '</div>' +
            '</div>' +
        '</div>';
    }

    $('#mailmodal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })

    $('#sidemodal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })
    
    $("#myBtn").click(function(){
        $('.toast').toast('show');
    });
});