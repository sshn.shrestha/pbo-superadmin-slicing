// $('.dropdown').hover(function() {
// 	$(this).find('.dropdown-menu').stop(true, true).delay(200).slideDown(200);
// }, function() {
// 	$(this).find('.dropdown-menu').stop(true, true).delay(200).slideUp(200);
// });
// enable to use nav on click

$(document).ready(function() {
    //Only needed for the filename of export files.
    //Normally set in the title tag of your page.document.title = 'Simple DataTable';
    //Define hidden columns
    var hCols = [3, 4];
    // DataTable initialisation
    $('#example').DataTable({
        // "dom": "<'row'<'col-2'l><'col-5'p<br/>i><'col-3'B><'col-2'p<br/>i>>",
        "dom": "<'row'<'col-4'l><'col-4'p i><'col-4'B>>" + "<'row'<'col-12'tr>>" + "<'row'<'col-12'i>>",
        "paging": true,
        "autoWidth": true,
        "columnDefs": [{
            "visible": true,
            "targets": hCols
        }],
        "buttons": [{
            extend: 'colvis',
            collectionLayout: 'two-column',
            text: function() {
                var totCols = $('#example tr th').length;
                var hiddenCols = hCols.length;
                var shownCols = totCols - hiddenCols;
                return 'Columns (' + shownCols + ' of ' + totCols + ')';
            },
            prefixButtons: [{
                extend: 'colvisGroup',
                text: 'Show all',
                show: ':hidden'
            }, {
                extend: 'colvisRestore',
                text: 'Restore'
            }]
        }, {
            extend: 'collection',
            text: 'Export',
            buttons: [{
                    text: 'Excel',
                    extend: 'excelHtml5',
                    footer: false,
                    exportOptions: {
                        columns: ':visible'
                    }
                }, {
                    text: 'CSV',
                    extend: 'csvHtml5',
                    fieldSeparator: ';',
                    exportOptions: {
                        columns: ':visible'
                    }
                }, {
                    text: 'PDF Portrait',
                    extend: 'pdfHtml5',
                    message: '',
                    exportOptions: {
                        columns: ':visible'
                    }
                }, {
                    text: 'PDF Landscape',
                    extend: 'pdfHtml5',
                    message: '',
                    orientation: 'landscape',
                    exportOptions: {
                        columns: ':visible'
                    }
                }]
            }]
        ,oLanguage: {
            oPaginate: {
                sNext: '<span class="pagination-default">&#x276f;</span>',
                sPrevious: '<span class="pagination-default">&#x276e;</span>'
            }
        },
        "initComplete": function(settings, json) {
            // Adjust hidden columns counter text in button -->
            $('#example').on('column-visibility.dt', function(e, settings, column, state) {
                var visCols = $('#example thead tr:first th').length;
                //Below: The minus 2 because of the 2 extra buttons Show all and Restore
                var tblCols = $('.dt-button-collection li[aria-controls=example] a').length - 2;
                $('.buttons-colvis[aria-controls=example] span').html('Columns (' + visCols + ' of ' + tblCols + ')');
                e.stopPropagation();
            });
        }
    });
});



$(document).ready(function() {
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );


    
